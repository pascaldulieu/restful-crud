College assignment to create a website which can manage a database

RESTful CRUD in Node.js for mySQL management.

## Installation
Clone or download zip to your machine :

    cd restful-crud

then

    npm install

## Database import

Create a database named PP

Take the SQL file and import it into the MYSQL database:
- Web Pannel: Go to database > import > upload file
- CLI: `mysql -u root -p PP < PP.sql` 

## Configuration (database)
server.js

go to line 23-31 and edit the connection function to fit your setup

        host: 'localhost', //if running locally set to local host if running remotly set to IP
        user: 'root',
        password : 'root', //DB Passowrd
        port : 3306, //mysql port
        database:'PP'	

## Running the server
Open your terminal and go to the root foler
```
cd restful-crud
```
Start the server
```
node .
```

## Open your Browser
IP:3000 or localhost:3000
