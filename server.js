var express = require('express'),
    path = require('path'),
    bodyParser = require('body-parser'),
    app = express(),
    expressValidator = require('express-validator');


/*Set EJS template Engine*/
app.set('views', './views');
app.set('view engine', 'ejs');

app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({
    extended: true
})); //support x-www-form-urlencoded
app.use(bodyParser.json());
app.use(expressValidator());

/*MySql connection*/
var connection = require('express-myconnection'),
    mysql = require('mysql');

app.use(
    connection(mysql, {
        host: '192.168.88.123',
        user: 'root',
        password: 'x3zXNJMTSzs&I^qx',
        database: 'PP',
        debug: false //set true if you wanna see debug logger
    }, 'request')
);

app.get('/', function (req, res) {
    res.render('index', {
        title: "RESTful CRUD Website",
    });
});

//RESTful route
var router = express.Router();

// middleware
router.use(function (req, res, next) {
    console.log(req.method, req.url);
    next();
});


var address = router.route('/address');
var address2 = router.route('/address/:address_id');
var customer = router.route('/customer');
var customer2 = router.route('/customer/:customer_id');
var customer_order = router.route('/customer_order');
var customer_order2 = router.route('/customer_order/:customer_order_id');
var item = router.route('/item');
var item2 = router.route('/item/:item_id');
var item_order = router.route('/item_order');
var item_order2 = router.route('/item_order/:order_id');
var staff = router.route('/staff');
var staff2 = router.route('/staff/:staff_id');
var supplier = router.route('/supplier');
var supplier2 = router.route('/supplier/:supplier_id');
var warehouse = router.route('/warehouse');
var warehouse2 = router.route('/warehouse/:warehouse_id');
var workshop = router.route('/workshop');
var workshop2 = router.route('/workshop/:workshop_id');


//Address

//show the CRUD interface | GET
address.get(function (req, res, next) {
    req.getConnection(function (err, conn) {
        if (err) return next("Cannot Connect");
        var query = conn.query('SELECT * FROM address', function (err, rows) {
            if (err) {
                console.log(err);
                return next("Mysql error, check your query");
            }
            res.render('address', {
                title: "RESTful CRUD Website",
                data: rows
            });
        });
    });
});

//post data to DB | POST
address.post(function (req, res, next) {
    //validation
    req.assert('address_line_1', 'Address is required').notEmpty();
    req.assert('post_code', 'A valid post code is required').notEmpty();
    req.assert('city', 'A city is required').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        res.status(422).json(errors);
        return;
    }
    //get data
    var data = {
        address_line_1: req.body.address_line_1,
        address_line_2: req.body.address_line_2,
        post_code: req.body.post_code,
        city: req.body.city,
    };
    //inserting into mysql
    req.getConnection(function (err, conn) {
        if (err) return next("Cannot Connect");
        var query = conn.query("INSERT INTO address set ? ", data, function (err, rows) {
            if (err) {
                console.log(err);
                return next("Mysql error, check your query");
            }
            res.sendStatus(200);
        });
    });
});

address2.all(function (req, res, next) {
    console.log("You need to smth about address2 Route ? Do it here");
    console.log(req.params);
    next();
});

//get data to update
address2.get(function (req, res, next) {
    var address_id = req.params.address_id;
    req.getConnection(function (err, conn) {
        if (err) return next("Cannot Connect");
        var query = conn.query("SELECT * FROM address WHERE address_id = ? ", [address_id], function (err, rows) {
            if (err) {
                console.log(err);
                return next("Mysql error, check your query");
            }
            //if address not found
            if (rows.length < 1)
                return res.send("address Not found");

            res.render('edit_address', {
                title: "Edit address",
                data: rows
            });
        });
    });
});

//update data
address2.put(function (req, res, next) {
    var address_id = req.params.address_id;
    //validation
    req.assert('address_line_1', 'Address is required').notEmpty();
    req.assert('post_code', 'A valid post code is required').notEmpty();
    req.assert('city', 'A city is required').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        res.status(422).json(errors);
        return;
    }
    //get data
    var data = {
        address_line_1: req.body.address_line_1,
        address_line_2: req.body.address_line_2,
        post_code: req.body.post_code,
        city: req.body.city,
    };
    //inserting into mysql
    req.getConnection(function (err, conn) {
        if (err) return next("Cannot Connect");
        var query = conn.query("UPDATE address set ? WHERE address_id = ? ", [data, address_id], function (err, rows) {
            if (err) {
                console.log(err);
                return next("Mysql error, check your query");
            }
            res.sendStatus(200);
        });
    });
});

//delete data
address2.delete(function (req, res, next) {
    var address_id = req.params.address_id;
    req.getConnection(function (err, conn) {
        if (err) return next("Cannot Connect");
        var query = conn.query("DELETE FROM address  WHERE address_id = ? ", [address_id], function (err, rows) {
            if (err) {
                console.log(err);
                return next("Mysql error, check your query");
            }
            res.sendStatus(200);
        });
    });
});











// Customer

//show the CRUD interface | GET
customer.get(function (req, res, next) {
    req.getConnection(function (err, conn) {
        if (err) return next("Cannot Connect");
        var query = conn.query('SELECT * FROM customer', function (err, rows) {
            if (err) {
                console.log(err);
                return next("Mysql error, check your query");
            }
            res.render('customer', {
                title: "RESTful CRUD Website",
                data: rows
            });
        });
    });
});

//post data to DB | POST
customer.post(function (req, res, next) {
    //validation
    req.assert('forename', 'forename is required').notEmpty();
    req.assert('surname', 'surname is required').notEmpty();
    req.assert('address_id', 'An address id is required').notEmpty();
    req.assert('phone', 'A phone number is required').notEmpty();
    req.assert('email', 'An email is required').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        res.status(422).json(errors);
        return;
    }
    //get data
    var data = {
        forename: req.body.forename,
        surname: req.body.surname,
        address_id: req.body.address_id,
        phone: req.body.phone,
        email: req.body.email,
    };
    //inserting into mysql
    req.getConnection(function (err, conn) {
        if (err) return next("Cannot Connect");
        var query = conn.query("INSERT INTO customer set ? ", data, function (err, rows) {
            if (err) {
                console.log(err);
                return next("Mysql error, check your query");
            }
            res.sendStatus(200);
        });
    });
});

customer2.all(function (req, res, next) {
    console.log("You need to smth about customer2 Route ? Do it here");
    console.log(req.params.customer_id);
    next();
});

//get data to update
customer2.get(function (req, res, next) {
    var customer_id = req.params.customer_id;
    req.getConnection(function (err, conn) {
        if (err) return next("Cannot Connect");
        var query = conn.query("SELECT * FROM customer WHERE customer_id = ? ", [customer_id], function (err, rows) {
            if (err) {
                console.log(err);
                return next("Mysql error, check your query");
            }
            //if customer not found
            if (rows.length < 1)
                return res.send("customer Not found");

            res.render('edit_customer', {
                title: "Edit customer",
                data: rows
            });
        });
    });
});

//update data
customer2.put(function (req, res, next) {
    var customer_id = req.params.customer_id;
    //validation
    req.assert('forename', 'forename is required').notEmpty();
    req.assert('surname', 'surname is required').notEmpty();
    req.assert('address_id', 'An address id is required').notEmpty();
    req.assert('phone', 'A phone number is required').notEmpty();
    req.assert('email', 'An email is required').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        res.status(422).json(errors);
        return;
    }
    //get data
    var data = {
        forename: req.body.forename,
        surname: req.body.surname,
        address_id: req.body.address_id,
        phone: req.body.phone,
        email: req.body.email,
    };
    //inserting into mysql
    req.getConnection(function (err, conn) {
        if (err) return next("Cannot Connect");
        var query = conn.query("UPDATE customer set ? WHERE customer_id = ? ", [data, customer_id], function (err, rows) {
            if (err) {
                console.log(err);
                return next("Mysql error, check your query");
            }
            res.sendStatus(200);
        });
    });
});

//delete data
customer2.delete(function (req, res, next) {
    var customer_id = req.params.customer_id;
    req.getConnection(function (err, conn) {
        if (err) return next("Cannot Connect");
        var query = conn.query("DELETE FROM customer  WHERE customer_id = ? ", [customer_id], function (err, rows) {
            if (err) {
                console.log(err);
                return next("Mysql error, check your query");
            }
            res.sendStatus(200);
        });
    });
});











// Customer order

//show the CRUD interface | GET
customer_order.get(function (req, res, next) {
    req.getConnection(function (err, conn) {
        if (err) return next("Cannot Connect");
        var query = conn.query('SELECT * FROM customer_order', function (err, rows) {
            if (err) {
                console.log(err);
                return next("Mysql error, check your query");
            }
            res.render('customer_order', {
                title: "RESTful CRUD Website",
                data: rows
            });
        });
    });
});

//post data to DB | POST
customer_order.post(function (req, res, next) {
    //validation
    req.assert('customer_id', 'customer_id is required').notEmpty();
    req.assert('staff_id', 'staff_id is required').notEmpty();
    req.assert('order_id', 'order_id is required').notEmpty();
    req.assert('order_date', 'order_date is required').notEmpty();
    req.assert('delivery_date', 'delivery_date is required').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        res.status(422).json(errors);
        return;
    }
    //get data
    var data = {
        customer_id: req.body.customer_id,
        staff_id: req.body.staff_id,
        order_id: req.body.order_id,
        order_date: req.body.order_date,
        delivery_date: req.body.delivery_date,
    };
    //inserting into mysql
    req.getConnection(function (err, conn) {
        if (err) return next("Cannot Connect");
        var query = conn.query("INSERT INTO customer_order set ? ", data, function (err, rows) {
            if (err) {
                console.log(err);
                return next("Mysql error, check your query");
            }
            res.sendStatus(200);
        });
    });
});

customer_order2.all(function (req, res, next) {
    console.log("You need to smth about customer_order2 Route ? Do it here");
    console.log(req.params);
    next();
});

//get data to update
customer_order2.get(function (req, res, next) {
    var customer_order_id = req.params.customer_order_id;
    req.getConnection(function (err, conn) {
        if (err) return next("Cannot Connect");
        var query = conn.query("SELECT * FROM customer_order WHERE customer_order_id = ? ", [customer_order_id], function (err, rows) {
            if (err) {
                console.log(err);
                return next("Mysql error, check your query");
            }
            //if customer_order not found
            if (rows.length < 1)
                return res.send("customer_order Not found");

            res.render('edit_customer_order', {
                title: "Edit customer_order",
                data: rows
            });
        });
    });
});

//update data
customer_order2.put(function (req, res, next) {
    var customer_order_id = req.params.customer_order_id;
    //validation
    req.assert('customer_id', 'customer_id is required').notEmpty();
    req.assert('staff_id', 'staff_id is required').notEmpty();
    req.assert('order_id', 'order_id is required').notEmpty();
    req.assert('order_date', 'order_date is required').notEmpty();
    req.assert('delivery_date', 'delivery_date is required').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        res.status(422).json(errors);
        return;
    }
    //get data
    var data = {
        customer_id: req.body.customer_id,
        staff_id: req.body.staff_id,
        order_id: req.body.order_id,
        order_date: req.body.order_date,
        delivery_date: req.body.delivery_date,
    };
    //inserting into mysql
    req.getConnection(function (err, conn) {
        if (err) return next("Cannot Connect");
        var query = conn.query("UPDATE customer_order set ? WHERE customer_order = ? ", [data, customer_order_id], function (err, rows) {
            if (err) {
                console.log(err);
                return next("Mysql error, check your query");
            }
            res.sendStatus(200);
        });
    });
});

//delete data
customer_order2.delete(function (req, res, next) {
    var customer_order_id = req.params.customer_order_id;
    req.getConnection(function (err, conn) {
        if (err) return next("Cannot Connect");
        var query = conn.query("DELETE FROM customer_order  WHERE customer_order = ? ", [customer_order_id], function (err, rows) {
            if (err) {
                console.log(err);
                return next("Mysql error, check your query");
            }
            res.sendStatus(200);
        });
    });
});









// item

//show the CRUD interface | GET
item.get(function (req, res, next) {
    req.getConnection(function (err, conn) {
        if (err) return next("Cannot Connect");
        var query = conn.query('SELECT * FROM item', function (err, rows) {
            if (err) {
                console.log(err);
                return next("Mysql error, check your query");
            }
            res.render('item', {
                title: "RESTful CRUD Website",
                data: rows
            });
        });
    });
});

//post data to DB | POST
item.post(function (req, res, next) {
    //validation
    req.assert('workshop_id', 'workshop_id is required').notEmpty();
    req.assert('quantity', 'quantity is required').notEmpty();
    req.assert('category', 'category is required').notEmpty();
    req.assert('Item', 'Item is required').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        res.status(422).json(errors);
        return;
    }
    //get data
    var data = {
        workshop_id: req.body.workshop_id,
        quantity: req.body.quantity,
        category: req.body.category,
        Item: req.body.Item,
    };
    //inserting into mysql
    req.getConnection(function (err, conn) {
        if (err) return next("Cannot Connect");
        var query = conn.query("INSERT INTO item set ? ", data, function (err, rows) {
            if (err) {
                console.log(err);
                return next("Mysql error, check your query");
            }
            res.sendStatus(200);
        });
    });
});

item2.all(function (req, res, next) {
    console.log("You need to smth about item2 Route ? Do it here");
    console.log(req.params);
    next();
});

//get data to update
item2.get(function (req, res, next) {
    var item_id = req.params.item_id;
    req.getConnection(function (err, conn) {
        if (err) return next("Cannot Connect");
        var query = conn.query("SELECT * FROM item WHERE item_id = ? ", [item_id], function (err, rows) {
            if (err) {
                console.log(err);
                return next("Mysql error, check your query");
            }
            //if item not found
            if (rows.length < 1)
                return res.send("item Not found");

            res.render('edit_item', {
                title: "Edit item",
                data: rows
            });
        });
    });
});

//update data
item2.put(function (req, res, next) {
    var item_id = req.params.item_id;
    //validation
    req.assert('workshop_id', 'workshop_id is required').notEmpty();
    req.assert('quantity', 'quantity is required').notEmpty();
    req.assert('category', 'category is required').notEmpty();
    req.assert('Item', 'Item is required').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        res.status(422).json(errors);
        return;
    }
    //get data
    var data = {
        workshop_id: req.body.workshop_id,
        quantity: req.body.quantity,
        category: req.body.category,
        Item: req.body.Item,
    };
    //inserting into mysql
    req.getConnection(function (err, conn) {
        if (err) return next("Cannot Connect");
        var query = conn.query("UPDATE item set ? WHERE item_id = ? ", [data, item_id], function (err, rows) {
            if (err) {
                console.log(err);
                return next("Mysql error, check your query");
            }
            res.sendStatus(200);
        });
    });
});

//delete data
item2.delete(function (req, res, next) {
    var item_id = req.params.item_id;
    req.getConnection(function (err, conn) {
        if (err) return next("Cannot Connect");
        var query = conn.query("DELETE FROM address  WHERE item_id = ? ", [item_id], function (err, rows) {
            if (err) {
                console.log(err);
                return next("Mysql error, check your query");
            }
            res.sendStatus(200);
        });
    });
});









// item order

//show the CRUD interface | GET
item_order.get(function (req, res, next) {
    req.getConnection(function (err, conn) {
        if (err) return next("Cannot Connect");
        var query = conn.query('SELECT * FROM item_order', function (err, rows) {
            if (err) {
                console.log(err);
                return next("Mysql error, check your query");
            }
            res.render('item_order', {
                title: "RESTful CRUD Website",
                data: rows
            });
        });
    });
});

//post data to DB | POST
item_order.post(function (req, res, next) {
    //validation
    req.assert('staff_id', 'staff_id is required').notEmpty();
    req.assert('item_id', 'item_id is required').notEmpty();
    req.assert('date', 'date is required').notEmpty();
    req.assert('name', 'name is required').notEmpty();
    req.assert('cost', 'cost is required').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        res.status(422).json(errors);
        return;
    }
    //get data
    var data = {
        staff_id: req.body.staff_id,
        item_id: req.body.item_id,
        date: req.body.date,
        name: req.body.name,
        cost: req.body.cost,
    };
    //inserting into mysql
    req.getConnection(function (err, conn) {
        if (err) return next("Cannot Connect");
        var query = conn.query("INSERT INTO item_order set ? ", data, function (err, rows) {
            if (err) {
                console.log(err);
                return next("Mysql error, check your query");
            }
            res.sendStatus(200);
        });
    });
});

item_order2.all(function (req, res, next) {
    console.log("You need to smth about item_order2 Route ? Do it here");
    console.log(req.params);
    next();
});

//get data to update
item_order2.get(function (req, res, next) {
    var order_id = req.params.order_id;
    req.getConnection(function (err, conn) {
        if (err) return next("Cannot Connect");
        var query = conn.query("SELECT * FROM item_order WHERE order_id = ? ", [order_id], function (err, rows) {
            if (err) {
                console.log(err);
                return next("Mysql error, check your query");
            }
            //if address not found
            if (rows.length < 1)
                return res.send("item_order Not found");

            res.render('edit_item_order', {
                title: "Edit item_order",
                data: rows
            });
        });
    });
});

//update data
address2.put(function (req, res, next) {
    var order_id = req.params.order_id;
    //validation
    req.assert('staff_id', 'staff_id is required').notEmpty();
    req.assert('item_id', 'item_id is required').notEmpty();
    req.assert('date', 'date is required').notEmpty();
    req.assert('name', 'name is required').notEmpty();
    req.assert('cost', 'cost is required').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        res.status(422).json(errors);
        return;
    }
    //get data
    var data = {
        staff_id: req.body.staff_id,
        item_id: req.body.item_id,
        date: req.body.date,
        name: req.body.name,
        cost: req.body.cost,
    };
    //inserting into mysql
    req.getConnection(function (err, conn) {
        if (err) return next("Cannot Connect");
        var query = conn.query("UPDATE item_order set ? WHERE order_id = ? ", [data, order_id], function (err, rows) {
            if (err) {
                console.log(err);
                return next("Mysql error, check your query");
            }
            res.sendStatus(200);
        });
    });
});

//delete data
item_order2.delete(function (req, res, next) {
    var order_id = req.params.order_id;
    req.getConnection(function (err, conn) {
        if (err) return next("Cannot Connect");
        var query = conn.query("DELETE FROM item_order  WHERE order_id = ? ", [order_id], function (err, rows) {
            if (err) {
                console.log(err);
                return next("Mysql error, check your query");
            }
            res.sendStatus(200);
        });
    });
});









// staff

//show the CRUD interface | GET
staff.get(function (req, res, next) {
    req.getConnection(function (err, conn) {
        if (err) return next("Cannot Connect");
        var query = conn.query('SELECT * FROM staff', function (err, rows) {
            if (err) {
                console.log(err);
                return next("Mysql error, check your query");
            }
            res.render('staff', {
                title: "RESTful CRUD Website",
                data: rows
            });
        });
    });
});

//post data to DB | POST
staff.post(function (req, res, next) {
    //validation
    req.assert('forename', 'forename is required').notEmpty();
    req.assert('surname', 'surname is required').notEmpty();
    req.assert('phone', 'phone is required').notEmpty();
    req.assert('address_id', 'address_id is required').notEmpty();
    req.assert('department', 'department is required').notEmpty();
    req.assert('email', 'email is required').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        res.status(422).json(errors);
        return;
    }
    //get data
    var data = {
        forename: req.body.forename,
        surname: req.body.surname,
        phone: req.body.phone,
        address_id: req.body.address_id,
        department: req.body.department,
        email: req.body.email,
    };
    //inserting into mysql
    req.getConnection(function (err, conn) {
        if (err) return next("Cannot Connect");
        var query = conn.query("INSERT INTO staff set ? ", data, function (err, rows) {
            if (err) {
                console.log(err);
                return next("Mysql error, check your query");
            }
            res.sendStatus(200);
        });
    });
});

staff2.all(function (req, res, next) {
    console.log("You need to smth about staff2 Route ? Do it here");
    console.log(req.params);
    next();
});

//get data to update
staff2.get(function (req, res, next) {
    var staff_id = req.params.staff_id;
    req.getConnection(function (err, conn) {
        if (err) return next("Cannot Connect");
        var query = conn.query("SELECT * FROM staff WHERE staff_id = ? ", [staff_id], function (err, rows) {
            if (err) {
                console.log(err);
                return next("Mysql error, check your query");
            }
            //if staff not found
            if (rows.length < 1)
                return res.send("staff Not found");

            res.render('edit_staff', {
                title: "Edit staff",
                data: rows
            });
        });
    });
});

//update data
staff2.put(function (req, res, next) {
    var staff_id = req.params.staff_id;
    //validation
    req.assert('forename', 'forename is required').notEmpty();
    req.assert('surname', 'surname is required').notEmpty();
    req.assert('phone', 'phone is required').notEmpty();
    req.assert('address_id', 'address_id is required').notEmpty();
    req.assert('department', 'department is required').notEmpty();
    req.assert('email', 'email is required').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        res.status(422).json(errors);
        return;
    }
    //get data
    var data = {
        forename: req.body.forename,
        surname: req.body.surname,
        phone: req.body.phone,
        address_id: req.body.address_id,
        department: req.body.department,
        email: req.body.email,
    };
    //inserting into mysql
    req.getConnection(function (err, conn) {
        if (err) return next("Cannot Connect");
        var query = conn.query("UPDATE staff set ? WHERE staff_id = ? ", [data, staff_id], function (err, rows) {
            if (err) {
                console.log(err);
                return next("Mysql error, check your query");
            }
            res.sendStatus(200);
        });
    });
});

//delete data
staff2.delete(function (req, res, next) {
    var staff_id = req.params.staff_id;
    req.getConnection(function (err, conn) {
        if (err) return next("Cannot Connect");
        var query = conn.query("DELETE FROM staff  WHERE staff_id = ? ", [staff_id], function (err, rows) {
            if (err) {
                console.log(err);
                return next("Mysql error, check your query");
            }
            res.sendStatus(200);
        });
    });
});









// supplier

//show the CRUD interface | GET
supplier.get(function (req, res, next) {
    req.getConnection(function (err, conn) {
        if (err) return next("Cannot Connect");
        var query = conn.query('SELECT * FROM supplier', function (err, rows) {
            if (err) {
                console.log(err);
                return next("Mysql error, check your query");
            }
            res.render('supplier', {
                title: "RESTful CRUD Website",
                data: rows
            });
        });
    });
});

//post data to DB | POST
supplier.post(function (req, res, next) {
    //validation
    req.assert('address_id', 'Address is required').notEmpty();
    req.assert('phone', 'phone is required').notEmpty();
    req.assert('email', 'email is required').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        res.status(422).json(errors);
        return;
    }
    //get data
    var data = {
        address_id: req.body.address_id,
        phone: req.body.phone,
        email: req.body.email,
        website: req.body.website,
    };
    //inserting into mysql
    req.getConnection(function (err, conn) {
        if (err) return next("Cannot Connect");
        var query = conn.query("INSERT INTO supplier set ? ", data, function (err, rows) {
            if (err) {
                console.log(err);
                return next("Mysql error, check your query");
            }
            res.sendStatus(200);
        });
    });
});

supplier2.all(function (req, res, next) {
    console.log("You need to smth about supplier2 Route ? Do it here");
    console.log(req.params);
    next();
});

//get data to update
supplier2.get(function (req, res, next) {
    var supplier_id = req.params.supplier_id;
    req.getConnection(function (err, conn) {
        if (err) return next("Cannot Connect");
        var query = conn.query("SELECT * FROM supplier WHERE supplier_id = ? ", [supplier_id], function (err, rows) {
            if (err) {
                console.log(err);
                return next("Mysql error, check your query");
            }
            //if supplier not found
            if (rows.length < 1)
                return res.send("supplier Not found");

            res.render('edit_supplier', {
                title: "Edit supplier",
                data: rows
            });
        });
    });
});

//update data
supplier2.put(function (req, res, next) {
    var supplier_id = req.params.supplier_id;
    //validation
    req.assert('address_id', 'Address is required').notEmpty();
    req.assert('phone', 'phone is required').notEmpty();
    req.assert('email', 'email is required').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        res.status(422).json(errors);
        return;
    }
    //get data
    var data = {
        address_id: req.body.address_id,
        phone: req.body.phone,
        email: req.body.email,
        website: req.body.website,
    };
    //inserting into mysql
    req.getConnection(function (err, conn) {
        if (err) return next("Cannot Connect");
        var query = conn.query("UPDATE supplier set ? WHERE supplier_id = ? ", [data, supplier_id], function (err, rows) {
            if (err) {
                console.log(err);
                return next("Mysql error, check your query");
            }
            res.sendStatus(200);
        });
    });
});

//delete data
supplier2.delete(function (req, res, next) {
    var supplier_id = req.params.supplier_id;
    req.getConnection(function (err, conn) {
        if (err) return next("Cannot Connect");
        var query = conn.query("DELETE FROM supplier  WHERE supplier_id = ? ", [supplier_id], function (err, rows) {
            if (err) {
                console.log(err);
                return next("Mysql error, check your query");
            }
            res.sendStatus(200);
        });
    });
});









// warehouse

//show the CRUD interface | GET
warehouse.get(function (req, res, next) {
    req.getConnection(function (err, conn) {
        if (err) return next("Cannot Connect");
        var query = conn.query('SELECT * FROM warehouse', function (err, rows) {
            if (err) {
                console.log(err);
                return next("Mysql error, check your query");
            }
            res.render('warehouse', {
                title: "RESTful CRUD Website",
                data: rows
            });
        });
    });
});

//post data to DB | POST
warehouse.post(function (req, res, next) {
    //validation
    req.assert('supplier_id', 'supplier_id is required').notEmpty();
    req.assert('quantity', 'quantity is required').notEmpty();
    req.assert('location', 'location required').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        res.status(422).json(errors);
        return;
    }
    //get data
    var data = {
        supplier_id: req.body.supplier_id,
        quantity: req.body.quantity,
        location: req.body.location,
    };
    //inserting into mysql
    req.getConnection(function (err, conn) {
        if (err) return next("Cannot Connect");
        var query = conn.query("INSERT INTO warehouse set ? ", data, function (err, rows) {
            if (err) {
                console.log(err);
                return next("Mysql error, check your query");
            }
            res.sendStatus(200);
        });
    });
});

warehouse2.all(function (req, res, next) {
    console.log("You need to smth about warehouse2 Route ? Do it here");
    console.log(req.params);
    next();
});

//get data to update
warehouse2.get(function (req, res, next) {
    var warehouse_id = req.params.warehouse_id;
    req.getConnection(function (err, conn) {
        if (err) return next("Cannot Connect");
        var query = conn.query("SELECT * FROM warehouse WHERE warehouse_id = ? ", [warehouse_id], function (err, rows) {
            if (err) {
                console.log(err);
                return next("Mysql error, check your query");
            }
            //if warehouse not found
            if (rows.length < 1)
                return res.send("warehouse Not found");

            res.render('edit_warehouse', {
                title: "Edit warehouse",
                data: rows
            });
        });
    });
});

//update data
warehouse2.put(function (req, res, next) {
    var warehouse_id = req.params.warehouse_id;
    //validation
    req.assert('supplier_id', 'supplier_id is required').notEmpty();
    req.assert('quantity', 'quantity is required').notEmpty();
    req.assert('location', 'location required').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        res.status(422).json(errors);
        return;
    }
    //get data
    var data = {
        supplier_id: req.body.supplier_id,
        quantity: req.body.quantity,
        location: req.body.location,
    };
    //inserting into mysql
    req.getConnection(function (err, conn) {
        if (err) return next("Cannot Connect");
        var query = conn.query("UPDATE warehouse set ? WHERE warehouse_id = ? ", [data, warehouse_id], function (err, rows) {
            if (err) {
                console.log(err);
                return next("Mysql error, check your query");
            }
            res.sendStatus(200);
        });
    });
});

//delete data
warehouse2.delete(function (req, res, next) {
    var warehouse_id = req.params.warehouse_id;
    req.getConnection(function (err, conn) {
        if (err) return next("Cannot Connect");
        var query = conn.query("DELETE FROM warehouse  WHERE warehouse_id = ? ", [warehouse_id], function (err, rows) {
            if (err) {
                console.log(err);
                return next("Mysql error, check your query");
            }
            res.sendStatus(200);
        });
    });
});









// workshop

//show the CRUD interface | GET
workshop.get(function (req, res, next) {
    req.getConnection(function (err, conn) {
        if (err) return next("Cannot Connect");
        var query = conn.query('SELECT * FROM workshop', function (err, rows) {
            if (err) {
                console.log(err);
                return next("Mysql error, check your query");
            }
            res.render('workshop', {
                title: "RESTful CRUD Website",
                data: rows
            });
        });
    });
});

//post data to DB | POST
workshop.post(function (req, res, next) {
    //validation
    req.assert('warehouse_id', 'warehouse_id is required').notEmpty();
    req.assert('quantity', 'quantity is required').notEmpty();
    req.assert('refill_date', 'refill_date is required').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        res.status(422).json(errors);
        return;
    }
    //get data
    var data = {
        warehouse_id: req.body.warehouse_id,
        quantity: req.body.quantity,
        refill_date: req.body.refill_date,
    };
    //inserting into mysql
    req.getConnection(function (err, conn) {
        if (err) return next("Cannot Connect");
        var query = conn.query("INSERT INTO workshop set ? ", data, function (err, rows) {
            if (err) {
                console.log(err);
                return next("Mysql error, check your query");
            }
            res.sendStatus(200);
        });
    });
});

workshop2.all(function (req, res, next) {
    console.log("You need to smth about workshop2 Route ? Do it here");
    console.log(req.params);
    next();
});

//get data to update
workshop2.get(function (req, res, next) {
    var workshop_id = req.params.workshop_id;
    req.getConnection(function (err, conn) {
        if (err) return next("Cannot Connect");
        var query = conn.query("SELECT * FROM workshop WHERE workshop_id = ? ", [workshop_id], function (err, rows) {
            if (err) {
                console.log(err);
                return next("Mysql error, check your query");
            }
            //if workshop not found
            if (rows.length < 1)
                return res.send("workshop Not found");

            res.render('edit_workshop', {
                title: "Edit workshop",
                data: rows
            });
        });
    });
});

//update data
workshop2.put(function (req, res, next) {
    var workshop_id = req.params.workshop_id;
    //validation
    req.assert('warehouse_id', 'warehouse_id is required').notEmpty();
    req.assert('quantity', 'quantity is required').notEmpty();
    req.assert('refill_date', 'refill_date is required').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        res.status(422).json(errors);
        return;
    }
    //get data
    var data = {
        warehouse_id: req.body.warehouse_id,
        quantity: req.body.quantity,
        refill_date: req.body.refill_date,
    };
    //inserting into mysql
    req.getConnection(function (err, conn) {
        if (err) return next("Cannot Connect");
        var query = conn.query("UPDATE workshop set ? WHERE workshop_id = ? ", [data, workshop_id], function (err, rows) {
            if (err) {
                console.log(err);
                return next("Mysql error, check your query");
            }
            res.sendStatus(200);
        });
    });
});

//delete data
workshop2.delete(function (req, res, next) {
    var workshop_id = req.params.workshop_id;
    req.getConnection(function (err, conn) {
        if (err) return next("Cannot Connect");
        var query = conn.query("DELETE FROM workshop  WHERE workshop_id = ? ", [workshop_id], function (err, rows) {
            if (err) {
                console.log(err);
                return next("Mysql error, check your query");
            }
            res.sendStatus(200);
        });
    });
});










//router
app.use('/api', router);

//start Server
var server = app.listen(3000, function () {

    console.log("Listening to port %s", server.address().port);

});